const path = require('path');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const history = require('connect-history-api-fallback');
const express = require('express');
const bodyParser = require('body-parser');
const app = require('./app');
const getConfig = require('../../config/webpack.config');

const environments = {
  NODE_ENV: process.env.NODE_ENV || 'production',
  PORT: process.env.PORT || '3000'
};

const webpackConfig = getConfig(environments.NODE_ENV);
const compiler = webpack(webpackConfig);

app.use(history());

if (environments.NODE_ENV === 'development') {
  app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    noInfo: true,
    quiet: false,
    stats: {
      assets: false,
      colors: true,
      version: false,
      hash: false,
      timings: false,
      chunks: false,
      chunkModules: false
    }
  }));
  app.use(webpackHotMiddleware(compiler));
} else {
  const buildAppPath = path.join(__dirname, '../../build').normalize();
  console.log(__dirname);
  app.use(express.static(buildAppPath));
}

app.use(bodyParser());
require('./router');

app.listen(environments.PORT, () => {
  console.log(`Server ${environments.NODE_ENV} started at port ${environments.PORT}`);
});
