const shortId = require('shortid');
const crypto = require('crypto');

const app = require('./app');
const { getValueFromStorage, setValueToStorage } = require('./utils/data');

const authCheck = async (req, res, next) => {
  const { headers } = req;
  const users = await getValueFromStorage('users') || [];

  const authorized = users.some(user => user.token = headers.token);

  if (authorized) {
    return next();
  }

  res.status(401).send({ message: 'Bad token' });
};

const createToken = (data) => {
  return crypto
    .createHash('md5')
    .update(data)
    .digest("hex");
};

app.post('/api/auth/register', async (req, res) => {
  const { username, password } = req.body || {};

  const users = await getValueFromStorage('users');
  const exists = users.some(user => user.username === username);
  if (exists) return res.status(422).send({ message: 'User with that username already exists'});

  const token = createToken(`${username}:${password}`);

  await setValueToStorage('users', [
    ...users, {
      id: shortId(),
      username,
      password,
      token
    }
  ]);
  res.send('OK');
});

app.post('/api/auth/login', async (req, res) => {
  const { username, password } = req.body || {};

  const users = await getValueFromStorage('users');
  const user = users.find(item => item.username === username && item.password === password);

  if (!user) return res.status(422).send({ message: 'Username or password is incorrect' });

  const token = createToken(`${username}:${password}`);
  res.status(200).send({ user, token })
});

app.get('/api/tests', async (req, res) => {
  const tests = await getValueFromStorage('tests');
  res.status(200).send({ tests })
});

app.post('/api/get', async (req, res) => {
  const { path } = req.body || {};
  const data = await getValueFromStorage(path);
  res.status(200).send(data)
});

app.post('/api/set', async (req, res) => {
  const { path, data } = req.body || {};
  await setValueToStorage(path, data);
  res.status(200).send('OK')
});
