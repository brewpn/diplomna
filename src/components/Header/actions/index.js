import { history } from '../../../utils';
import { ROUTES } from '../../../constants';

import { headerStore } from '../store';
import { sessionStore } from '../../../stores';

export const redirectToLogin = () => {
  history.push(ROUTES.login)
};

export const redirectToProfile = () => {
  history.push(ROUTES.profile)
};

export const redirectHome = () => {
  history.push(ROUTES.home)
};

export const showHeader = () => {
  headerStore.show();
};

export const hideHeader = () => {
  headerStore.hide();
};

export const onLogout = () => {
  sessionStore.logout();
};
