import { action, observable } from 'mobx';

class Header {
  @observable
  display = true;

  @action
  show() {
    this.display = true
  }

  @action
  hide() {
    this.display = false
  }
}

const headerStore = new Header();
export { headerStore }
