import * as React from 'react';
import { observer } from 'mobx-react';

import { headerStore } from './store';
import { sessionStore } from '../../stores';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import MButton from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';

import { redirectToLogin, redirectHome, redirectToProfile, onLogout } from './actions';

const Button = withStyles(theme => ({
  root: {
    marginLeft: theme.spacing(2),
    color: 'white',
    borderColor: 'white'
  }
}))(MButton);

@withStyles({
  logo: {
    cursor: 'pointer'
  }
})
@observer
class Header extends React.Component {
  render() {
    const { classes } = this.props;

    if (!headerStore.display) {
      return null;
    }

    return (
      <AppBar position="static" color="primary">
        <Container maxWidth="md" disableGutters>
          <Toolbar disableGutters>
            <Box display='flex' justifyContent="space-between" width="100%">
              <Typography className={classes.logo} variant="h6" onClick={redirectHome}>
                TestApp
              </Typography>
              {!sessionStore.isLoggedIn &&
                <Button variant="outlined" onClick={ redirectToLogin }>
                  Login
                </Button>
              }
              {sessionStore.isLoggedIn &&
                <Box display="flex">
                  <MButton variant="contained" onClick={ redirectToProfile }>
                    Profile
                  </MButton>
                  <Button variant="outlined" onClick={ onLogout }>
                    Logout
                  </Button>
                </Box>
              }
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    )
  }
}

export { Header }
