import * as React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const styles = (theme) => ({
  heroContent: {
    padding: theme.spacing(8, 1, 6),
  }
});

@withStyles(styles)
class Layout extends React.Component {
  render() {
    const { children, classes } = this.props;

    return (
      <Container maxWidth="md" className={classes.heroContent}>
        {children}
      </Container>
    )
  }
}

export { Layout };
