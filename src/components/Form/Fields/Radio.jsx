import * as React from 'react';

import MRadio from '@material-ui/core/Radio';

export class Radio extends React.Component {
  static defaultProps = {
    fullWidth: true,
    rowsMax: 1
  };

  render() {
    const {
      label,
      name,
      value,
      option,
      onChange,
      className,
    } = this.props;

    return (
      <MRadio
        className={className}
        name={name}
        label={label}
        checked={option === value}
        value={option}
        onChange={(e) => {
          onChange({name, value: option}, e)
        }}
        margin="normal"
      />
    );
  }
}
