import * as React from 'react';

import MCheckbox from '@material-ui/core/Checkbox';

export class Checkbox extends React.Component {
  static defaultProps = {
    fullWidth: true,
    rowsMax: 1,
    value: []
  };

  render() {
    const {
      label,
      name,
      value,
      option,
      onChange,
      className,
    } = this.props;
    const checked = (value || []).some(item => item === option);

    return (
      <MCheckbox
        className={className}
        name={name}
        label={label}
        checked={checked}
        value={option}
        onChange={(e) => {
          onChange({name, value: option}, e)
        }}
        margin="normal"
      />
    );
  }
}
