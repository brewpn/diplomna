import * as React from 'react';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateMomentUtils from '@date-io/moment'

export class Date extends React.Component {
  static defaultProps = {
    format: 'DD/MM/YYYY',
    variant: 'inline',
  };

  render() {
    const {
      disableToolbar,
      variant,
      format,
      name,
      label,
      value,
      errorMessage,
      onChange
    } = this.props;

    return (
      <MuiPickersUtilsProvider utils={DateMomentUtils}>
        <KeyboardDatePicker
          id={name}
          disableToolbar={disableToolbar}
          variant={variant}
          format={format}
          margin="normal"
          label={label}
          value={value}
          onChange={(value) => {
            onChange({name, value})
          }}
          error={!!errorMessage}
          helperText={errorMessage}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
      </MuiPickersUtilsProvider>
    )
  }
}
