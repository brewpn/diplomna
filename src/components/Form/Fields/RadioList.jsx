import * as React from 'react';

import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';

class RadioList extends React.Component {
  static defaultProps = {
    options: []
  };

  isChecked = (id) => {
    const { value } = this.props;
    return (value || {}).id === id
  };

  renderRadioList = () => {
    const { options, name, onChange, disabled } = this.props;

    return options.map(option => (
      <FormControlLabel
        key={`checkbox:${name}:${option.id}`}
        control={(
          <Radio
            checked={this.isChecked(option.id)}
            value={option}
            onChange={e => {
              onChange({name, value: option}, e)
            }}
            disabled={disabled}
            name={`${name}:${option.id}`}
          />
        )}
        label={option.title}
      />
    ))
  };

  render() {
    const {
      label,
      errorMessage,
      className
    } = this.props;

    return (
      <FormControl component="fieldset" error={!!errorMessage} className={className}>
        {!!label &&
        <FormLabel component="legend">{ label }</FormLabel>
        }
        <FormGroup>
          {this.renderRadioList()}
        </FormGroup>
        {!!errorMessage &&
        <FormHelperText>{ errorMessage }</FormHelperText>
        }
      </FormControl>
    )
  }
}

export { RadioList }
