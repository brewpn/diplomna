import * as React from 'react';
import _ from 'lodash';

import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

class CheckboxList extends React.Component {
  static defaultProps = {
    options: [],
    value: []
  };

  isChecked = (id) => {
    const { value } = this.props;
    return !!(value || []).find(item => item.id === id)
  };

  onChange = (option) => (e) => {
    const { onChange, name, value } = this.props;
    const copy = _.cloneDeep(value);
    const alreadyChecked = !!value.find(item => item.id === option.id);

    if (alreadyChecked) {
      _.remove(copy, item => item.id === option.id)
    } else {
      copy.push(option);
    }

    onChange({name, value: copy}, e)
  };

  renderCheckboxList = () => {
    const { options, name, disabled } = this.props;

    return options.map(option => (
      <FormControlLabel
        key={`checkbox:${name}:${option.id}`}
        control={(
          <Checkbox
            checked={this.isChecked(option.id)}
            value={option.id}
            onChange={this.onChange(option)}
            disabled={disabled}
            name={`${name}:${option.id}`}
          />
        )}
        label={option.title}
      />
    ))
  };

  render() {
    const {
      label,
      errorMessage,
      className
    } = this.props;

    return (
      <FormControl component="fieldset" error={!!errorMessage} className={className}>
        {!!label &&
          <FormLabel component="legend">{ label }</FormLabel>
        }
        <FormGroup>
          {this.renderCheckboxList()}
        </FormGroup>
        {!!errorMessage &&
          <FormHelperText>{ errorMessage }</FormHelperText>
        }
      </FormControl>
    )
  }
}

export { CheckboxList }
