import * as React from 'react';
import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/mode-java';
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/mode-csharp';
import 'ace-builds/src-noconflict/mode-python';

import 'ace-builds/src-noconflict/theme-tomorrow';
import 'ace-builds/src-noconflict/ext-language_tools'

import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {withStyles} from '@material-ui/core/styles';
import FormHelperText from '@material-ui/core/FormHelperText';

@withStyles(theme => ({
  select: {
    width: '200px',
    marginBottom: theme.spacing(2)
  },
  form: {
    padding: theme.spacing(2),
  }
}))
class CodeEditor extends React.Component {
  static defaultProps = {
    value: ''
  };

  state = {
    mode: 'javascript'
  };

  handleChangeMode = (e) => {
    this.setState({
      mode: e.target.value
    })
  };

  render() {
    const {
      name,
      value,
      onChange,
      classes,
      errorMessage,
      disabled
    } = this.props;
    const { mode } = this.state;

    return (
      <FormControl className={classes.form} fullWidth>
        <Select
          value={mode}
          className={classes.select}
          onChange={this.handleChangeMode}
          variant="outlined"
        >
          <MenuItem value="javascript">JavaScript</MenuItem>
          <MenuItem value="csharp">C#</MenuItem>
          <MenuItem value="python">Python</MenuItem>
          <MenuItem value="java">Java</MenuItem>
        </Select>
        <AceEditor
          mode={mode}
          theme="tomorrow"
          width="90%"
          value={ value }
          onChange={(code) => {
            onChange({name, value: code})
          }}
          name={name}
          readOnly={disabled}
          editorProps={{$blockScrolling: true}}
          setOptions={{
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            enableSnippets: true
          }}
        />
        <FormHelperText error={!!errorMessage}>{ errorMessage }</FormHelperText>
      </FormControl>
    )
  }
}

export { CodeEditor }
