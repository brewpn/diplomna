import * as React from 'react';
import _ from 'lodash';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import MUISelect from '@material-ui/core/Select';

export class Select extends React.Component {
  static defaultProps = {
    options: [],
    value: {}
  };

  renderOptions = () => {
    const { options, name } = this.props;
    if (_.isEmpty(options)) {
      return (
        <MenuItem disabled value="">No results</MenuItem>
      )
    }

    return options.map(option => (
      <MenuItem
        key={`menu-item:${name}:${option.id}`}
        value={option.id}
      >
        {option.title}
      </MenuItem>
    ));
  };

  render() {
    const {
      name,
      value,
      onChange,
      label,
      options,
      errorMessage
    } = this.props;
    const labelId = `${name}:label`;

    return (
      <FormControl variant="outlined" error={!!errorMessage} fullWidth>
        <InputLabel id={labelId}>{label}</InputLabel>
        <MUISelect
          labelId={labelId}
          value={value.id || ''}
          onChange={e => {
            const value = options.find(item => item.id === e.target.value) || {};
            onChange({ name, value }, e)
          }}
          label={label}
          fullWidth
        >
          {this.renderOptions()}
        </MUISelect>
        {errorMessage &&
          <FormHelperText>{errorMessage}</FormHelperText>
        }
      </FormControl>
    )
  }
}
