import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class Input extends React.Component {
  static defaultProps = {
    fullWidth: true,
    rowsMax: 1
  };

  render() {
    const {
      label,
      name,
      value = '',
      type,
      onChange,
      fullWidth,
      errorMessage,
      rows,
      rowsMax,
      className,
      multiline,
      disabled
    } = this.props;

    return (
      <TextField
        className={className}
        name={name}
        label={label}
        value={value}
        type={type}
        onChange={(e) => {
          onChange({name, value: e.target.value}, e)
        }}
        fullWidth={fullWidth}
        margin="normal"
        variant="outlined"
        disabled={disabled}

        multiline={multiline}
        rows={rows}
        rowsMax={rowsMax}

        error={!!errorMessage}
        helperText={errorMessage}
      />
    );
  }
}
