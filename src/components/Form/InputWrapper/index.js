import * as React from 'react';
import {observer} from 'mobx-react';
import shortid from 'shortid';
import isArray from 'lodash/isArray';

import {errorsStore} from '../../../stores';
import {FormContext} from '../FormWrapper';

@observer
class InputWrapper extends React.Component {
  static defaultProps = {settings: {}};
  static contextType = FormContext;

  state = (() => {
    const {settings, name} = this.props;
    const {id} = settings;
    const lodashName = name.split('.');

    if (!!id) {
      return {inputId: null, lodashName};
    }

    return {inputId: shortid(), lodashName};
  })();

  componentDidMount() {
    const {lodashName} = this.state;
    this.context.form && this.context.form.registerField(lodashName);
  }

  static getDerivedStateFromProps(props, state) {
    const lodashName = props.name.split('.');

    return {
      ...state,
      lodashName
    }
  }

  inputProps = () => {
    const {name, settings} = this.props;
    const {inputId} = this.state;

    return {name, ...settings, id: settings.id || inputId};
  };

  generateAutoComplete = () => {
    const disableAutofill = this.context.form?.disableAutofill;

    if (!disableAutofill) {
      return {};
    }

    const autoComplete = 'new-password';
    return {autoComplete};
  };

  value = () => {
    const {name} = this.props;
    return this.context.form && this.context.form.fetch(name);
  };

  error = () => {
    const {name} = this.props;
    const formName = this.context.form && this.context.form.id;

    if (!formName || !name) {
      return '';
    }

    const errors = errorsStore.get(formName, name);
    if (!isArray(errors)) {
      return '';
    }

    return errors[0];
  };

  onChange = (changes, e) => {
    const {lodashName} = this.state;

    if (this.context.form) {
      this.context.form.onChange({...changes, name: lodashName});
    }
  };

  render() {
    const {Component} = this.props;
    const value = this.value();
    const error = this.error();
    const autoCompleteOptions = this.generateAutoComplete();

    return (
      <Component
        onChange={this.onChange}
        value={value}
        errorMessage={error}
        {...autoCompleteOptions}
        {...this.inputProps()}
      />
    );
  }
}

export {InputWrapper};
