import * as React from 'react';
import {form} from '../../../stores';

const FormContext = React.createContext({});

class FormWrapper extends React.Component {
  static defaultProps = {
    structure: {},
    withScroll: false,
    disableAutofill: false
  };

  componentWillUnmount() {
    const {id} = this.props;
    form.clean(id);
  }

  contextProps = () => {
    const {id, withScroll = false, disableAutofill} = this.props;

    return {
      form: {
        store: form,
        id,
        fetch: this.fetch,
        onChange: this.onChange,
        registerField: this.registerField,
        withScroll,
        disableAutofill: !!disableAutofill
      }
    };
  };

  onChange = ({name, value}) => {
    const {id} = this.props;
    form.onChange(id, name, value);
  };

  registerField = (name) => {
    const {id} = this.props;
    form.registerField(id, name);
  };

  fetch = (name) => {
    const {id} = this.props;
    return form.fetch(id, name);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const {onSubmit} = this.props;

    if (onSubmit) {
      onSubmit();
    }
  };

  generateAutoComplete = () => {
    const {disableAutofill} = this.props;

    if (!disableAutofill) {
      return {};
    }

    return {autoComplete: 'off'};
  };

  render() {
    const {children} = this.props;
    const autoCompleteOptions = this.generateAutoComplete();

    const contextProps = this.contextProps();
    return (
      <FormContext.Provider value={contextProps}>
        <form onSubmit={this.handleSubmit} {...autoCompleteOptions}>
          {children}
        </form>
      </FormContext.Provider>
    );
  }
}

export {FormWrapper, FormWrapper as Form, FormContext};
