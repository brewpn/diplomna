import * as React from 'react';

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#48466d'
    },
    secondary: {
      main: '#3d84a8'
    },
    text: {
      main: '#000',
      secondary: '#a8aeb8'
    },

    labelGrey: '#a8aeb8',
    textPrimary: '#262e35'
  },

  overrides: {

  }
});

export class Theme extends React.Component {
  render() {
    const { children } = this.props;

    return (
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    )
  }
}
