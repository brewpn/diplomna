import _ from 'lodash';
import axios from 'axios';
import { request, action } from '../utils';

class Server {
  @request({ action: 'SERVER_GET' })
  async getStorageRequest(path) {
    return axios.post('/api/get', { path });
  }

  @action({ action: 'SERVER_GET'})
  async getStorage(path) {
    const [,response] = await this.getStorageRequest(path);

    return response.data
  }

  @request({ action: 'SERVER_SET' })
  async setStorageRequest(path, data) {
    return axios.post('/api/set', { path, data });
  }

  @action({ action: 'SERVER_SET'})
  async setStorage(path, data) {
    const [,response] = await this.setStorageRequest(path, data);

    return response.data
  }
}

const server = new Server();

const isServer = true;

export const getValueFromStorage = async (path) => {
  const tests = await server.getStorage(path);
  return tests;
  if (isServer) {
    return await server.getStorage(path);
  }

  const storageJSON = await localStorage.getItem('storage') || '{}';
  let storage;

  try {
    storage = JSON.parse(storageJSON)
  } catch {
    storage = {};
  }

  if (path) {
    return _.get(storage, path)
  }

  return storage
};

export const setValueToStorage = async (path, value) => {
  if (isServer) {
    return await server.setStorage(path, value);
  }

  if (!path) {
    throw new Error('You should define the path')
  }

  const storage = await getValueFromStorage();
  _.set(storage, path, value);

  localStorage.setItem('storage', JSON.stringify(storage));
};
