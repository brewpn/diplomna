import { errorsStore, sessionStore } from '../stores';

class ResponseProcessor {
  constructor(params) {
    const { action, response, root, writeError } = params;
    this.action = action;
    this.response = response;
    this.root = root;
    this.writeError = writeError;
  }

  async process() {
    if (!this.response) {
      console.error('Provided invalid response object, response processing will not be performed');
      return;
    }
    try {
      this.processByStatus();
      this.processNewToken();
    } catch (e) {
      console.error(e);
    }
  }

  processNewToken() {
    // const headers = (this.response.headers: any);
    // const token = headers && headers['authorization'];
    // if (token)
    //   sessionStore.debouncedSetToken(token);
  }

  processByStatus() {
    switch (this.response.status) {
      case 422:
        this.handleValidationError();
        return;
      case 401:
        this.handleUnauthorized();
        return;
      case 404:
        return;
      case 403:
        this.handleValidationError();
        return;
      case 500:
        console.error('Internal server error', this.response);
        // toastService.error('Server is not responding to your request, please contact support@race.se');
        return;
      default:
        return;
    }
  }

  handleUnauthorized() {
    sessionStore.logout();
  }

  handleValidationError() {
    if (this.writeError && this.response && this.response.data) {
      errorsStore.add(this.action, this.response.data[this.root]);
    }
  }
}

export { ResponseProcessor };
