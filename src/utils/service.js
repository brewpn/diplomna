import { ResponseProcessor } from './responseProcessor';

import { progressStore, errorsStore } from '../stores';

// import { Session } from 'services/session';

// request annotation
export const request = (reqArgs) => {
  reqArgs = { root: 'errors', ...reqArgs, writeError: true };

  return (target, propertyKey, descriptor) => {
    let oldValue = descriptor.value;
    // const sessionService = new Session(sessionStore);

    descriptor.value = async function(...args) {
      try {
        // await sessionService.refreshTokenIfNeeded();

        const response = await oldValue.call(this, ...args);
        new ResponseProcessor({ ...reqArgs, response }).process();

        return [true, response];
      } catch (e) {
        new ResponseProcessor({ ...reqArgs, response: e.response }).process();
        return [false, e.response];
      }
    };

    return descriptor;
  };
};

export function action(reqArgs) {
  reqArgs = { ...reqArgs, writeError: true };

  return function(target, propertyKey, descriptor) {
    let oldValue = descriptor.value;

    descriptor.value = async function(...args) {
      const action = reqArgs.action;

      if (reqArgs.onlyFirst && progressStore.isLoading(action)) {
        return false;
      }

      progressStore.log(action, 'progress');
      errorsStore.clear(action);

      const requestStartTime = new Date();

      const status = await oldValue.call(this, ...args);

      let logStatus = 'completed';
      if (status === false || logStatus === true) {
        logStatus = !!status ? 'completed' : 'failed';
      }

      let timeProgressParams = null;

      if (reqArgs.minRequestTime) {
        timeProgressParams = {
          requestStartTime,
          minRequestTime: reqArgs.minRequestTime
        };
      }

      progressStore.log(action, logStatus, timeProgressParams);
      return status;
    };

    return descriptor;
  };
}
