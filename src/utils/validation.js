import validate from 'validate.js'
import isFunction from 'lodash/isFunction';
import isEmpty from 'lodash/isEmpty';

import { errorsStore } from '../stores';

const CONSTRAINTS_SCHEME_KEY = '_validation';
class Validator {

  static validate(object, validator) {
    if (!object || !validator)
      return;

    if (isFunction(validator)) {
      return validate(object, validator(object));
    } else {
      return validate(object, validator);
    }
  }

  constructor(dataForValidation, validationScheme, prefix, skipRule) {
    this.validationScheme = validationScheme;
    this.prefix = prefix;
    this.dataForValidation = dataForValidation;
    this.errors = undefined;
    this.skipRule = skipRule;

    const errorKeyPrefix = prefix ? `${prefix}.` : '';

    this.validate(dataForValidation, validationScheme, errorKeyPrefix);
  }

  validate(dataForValidation, schema = this.validationScheme, prefix = '') {
    for(let [key, nestedSchema] of Object.entries(schema)) {

      if (key === CONSTRAINTS_SCHEME_KEY) {

        if (dataForValidation instanceof Array) {
          this.validateArray(dataForValidation, nestedSchema, prefix);
        } else {
          this.validateObject(dataForValidation, nestedSchema, prefix);
        }

      } else {
        if (dataForValidation instanceof Array) {
          dataForValidation.forEach((objectForValidation, index) => {
            const nestedErrorKeyPrefix = `${prefix}${index}.${key}.`;
            this.validate(objectForValidation[key], nestedSchema, nestedErrorKeyPrefix);
          });
        } else {
          const nestedErrorKeyPrefix = `${prefix}${key}.`;

          if (!dataForValidation[key]) {
            return;
          }

          this.validate(dataForValidation[key], nestedSchema, nestedErrorKeyPrefix);
        }
      }
    }
  }

  validateObject(
    dataForValidation,
    schema = this.validationScheme,
    prefix = ''
  ) {
    if (dataForValidation instanceof Array || !dataForValidation) {
      return;
    }

    const isSkipped = this.skipRule(dataForValidation, prefix);
    if (isSkipped) {
      return;
    }

    const errors = Validator.validate(dataForValidation, schema);
    if (!errors) {
      return;
    }

    this.addErrors(errors, prefix);
  }

  validateArray(dataForValidation, schema = this.validationScheme, prefix = '') {
    if (!(dataForValidation instanceof Array) || !dataForValidation) {
      return;
    }

    dataForValidation.forEach((obj, index) => {
      this.validateObject(obj, schema, `${prefix}${index}.`);
    });
  }

  addErrors(errors , prefix = '') {
    if (!this.errors) {
      this.errors = {};
    }

    const errorKeys = Object.keys(errors);

    errorKeys.forEach(key => {
      this.errors[`${prefix}${key}`] = errors[key];
    });
  }
}

export { Validator };


export function setNestedValidation(
  action,
  schema,
  objectForValidation,
  skipRule = () => false
) {
  const validator = new Validator(objectForValidation, schema, '', skipRule);
  const errors = validator.errors;

  if (!isEmpty(errors)) {
    errorsStore.add(action, errors);

    return isEmpty(errors);
  }

  return true;
}