import { action, request } from '../utils';
import { INITIALIZE_APP } from '../constants';

import { sessionStore } from '../stores/session';

class AppService {
  @request({ action: 'USER_PROFILE' })
  async getUserDataRequest() {
    return [true, {
      username: 'Brewpn'
    }]
  }

  @action({ action: INITIALIZE_APP })
  async initializeApp() {
    const token = sessionStore.tokenFromStorage;

    const [status, response] = await this.getUserDataRequest(token);

    if (status) {
      sessionStore.login(response.data.data, token)
    }
  }
}

const appService = new AppService();
export { appService };
