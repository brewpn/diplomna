export const ROUTES = {
  home: '/',
  auth: '/auth',
  login: '/auth/login',
  register: '/auth/register',
  createTest: '/create',
  testDetails: '/test/:id',
  profile: '/profile',
  profileTest: '/profile/test/:id'
};
