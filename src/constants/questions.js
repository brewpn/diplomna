export const QUESTION_TYPES = [{
  id: 1,
  title: 'Checkbox'
}, {
  id: 2,
  title: 'Radio'
}, {
  id: 3,
  title: 'Number'
}, {
  id: 4,
  title: 'Code'
}];