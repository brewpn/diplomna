import { appService } from '../services/app';
import { sessionStore } from '../stores/session';

export const initializeApp = async () => {
  if (sessionStore.isLoggedIn) {
    await appService.getUserDataRequest();
  }
};
