import {observable, action} from 'mobx';
import {set, get, cloneDeep, merge, isNil} from 'lodash';

// Note, storing form data
class Form {
  @observable value = {};

  @action
  registerField(id, name) {
    this.registerForm(id);

    const currentValue = this.fetch(id, name);

    if (isNil(currentValue)) {
      this.onChange(id, name, undefined);
    }
  }

  @action
  registerForm(id) {
    const currentValue = this.fetch(id);

    if (!isNil(currentValue)) {
      return;
    }

    this.value[id] = {};
  }

  @action
  clean(id) {
    this.value[id] = undefined;
  }

  // id - form id
  // name - full nested path for value
  // inputValue - value for change
  @action
  onChange(id, name, inputValue) {
    const {value} = this;
    set(value[id], name, inputValue);
  }

  @action
  async merge(id, value) {
    this.value[id] = merge(this.value[id], value);
  }

  // id - form id
  // name - full nested path for value
  fetch(id, name) {
    const {value} = this;
    if (!name) {
      return (value[id]);
    }

    return get(value[id], name);
  }

// When component is marked as observer
// but component is not have to subscribe for this store
  fetchWithoutObservable(id, name) {
    const {value} = this;
    if (!name) {
      return (cloneDeep(value[id]));
    }

    return get(cloneDeep(value[id]), name);
  }
}

const form = new Form();
export {Form, form};
