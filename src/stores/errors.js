import { observable, action } from 'mobx';
import { unset, get } from 'lodash';

import { Errors as ErrorUtil } from '../utils';

class Errors {
  @observable
  erroribus = {};

  @action
  add(action, values) {
    const oldValues = this.erroribus[action] || {};
    this.erroribus[action] = {
      ...ErrorUtil.mergeValues(oldValues, ErrorUtil.unfold(values))
    }
  }

  @action
  clean(action, ...keys) {
    const values = this.erroribus[action];
    keys.forEach(key => {
      unset(values, key);
    });

    this.erroribus[action] = values;
  }

  @action
  clear(action) {
    this.erroribus[action] = null;
  }

  get(action, key) {
    return get(this.erroribus[action] || {}, key);
  }
}

const errorsStore = new Errors();
export { errorsStore }
