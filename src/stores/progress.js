import { action, observable } from 'mobx';

class Progress {
  @observable
  progress = {};

  @action
  log(action, status, timeout) {
    const commit = () => {
      this.commitLog(action, status);
    };

    if (!timeout) {
      commit();
      return;
    }

    const { requestStartTime, minRequestTime } = timeout;
    const endTime = new Date();
    const requestTime = (requestStartTime - endTime) / 1000;

    if (requestTime > minRequestTime) {
      commit();
      return;
    }

    setTimeout(() => {
      commit();
    }, minRequestTime - requestTime);
  }

  @action
  commitLog(action, status) {
    this.progress[action] = { status: status };
  }

  isLoading(key) {
    const progress = this.progress[key];
    return !!(progress && progress.status === 'progress');
  }
}

const progressStore = new Progress();
export { progressStore }
