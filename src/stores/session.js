import { action, observable, computed } from 'mobx';
import axios from 'axios';

class SessionStore {
  constructor() {
    const token = localStorage.getItem('token');
    let user = {};

    try {
      user = JSON.parse(localStorage.getItem('user') || '{}');
    } catch {
      user = {};
    }

    this.token = token;
    this.user = user;
  }


  @observable
  user = {};
  @observable
  token = null;

  @action
  login(user, token) {
    this.user = user;
    this.token = token;

    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));

    axios.defaults.headers.common['token'] = token
  }

  @action
  logout() {
    this.user = {};
    this.token = null;

    localStorage.removeItem('token');
    localStorage.removeItem('user');

    axios.defaults.headers.common['token'] = undefined
  }

  @computed
  get isLoggedIn() {
    return !!this.token
  }

  @computed
  get tokenFromStorage() {
    return localStorage.getItem('token')
  }
}

const sessionStore = new SessionStore();
export { sessionStore };
