import * as React from 'react';
import { Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import { history } from './utils';
import { Header } from './components/Header';
import { User } from './router';
import { Theme } from './components/Theme';

import { initializeApp } from './actions/appLifecycle';

export class App extends React.Component {
  componentDidMount() {
    initializeApp();
  }

  render() {
    return (
      <Theme>
        <ToastContainer/>
        <Router history={history}>
          <Header/>
          <User/>
        </Router>
      </Theme>
    )
  }
}
