import { action, getValueFromStorage } from '../../../utils';
import { sessionStore } from '../../../stores';
import { LOAD_USER_TESTS } from '../constants';
import { profileStore } from '../stores';

class Profile {
  @action({ action: LOAD_USER_TESTS })
  async getUserTests() {
    const tests = await getValueFromStorage('tests') || [];
    const filteredTests = tests.filter(item => item.user_id === sessionStore.user.id);

    profileStore.setTests(filteredTests)
  }
}

const profileService = new Profile();
export { profileService }
