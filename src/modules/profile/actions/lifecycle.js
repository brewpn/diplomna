import { profileService } from '../requests';
import { profileStore } from '../stores';

export const mount = async () => {
  await profileService.getUserTests()
};

export const unmount = () => {
  profileStore.clear();
};
