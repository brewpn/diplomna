import { generatePath } from 'react-router-dom';

import { history } from '../../../utils';
import { ROUTES } from '../../../constants';

export const onClickTest = (id) => () => {
  const path = generatePath(ROUTES.profileTest, { id });
  history.push(path);
};
