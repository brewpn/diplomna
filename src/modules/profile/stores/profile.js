import {action, observable} from 'mobx';

class Profile {
  @observable
  tests = [];

  @action
  setTests(data) {
    this.tests = data
  }

  @action
  clear() {
    this.tests = [];
  }
}

const profileStore = new Profile();
export { profileStore }
