import * as React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';

import Typography from '@material-ui/core/Typography';

import { Layout } from '../../../components/Layout';
import {mount, unmount} from '../actions';
import { profileStore } from '../stores';
import {onClickTest} from '../actions';
import { sessionStore } from '../../../stores';

import Card from '@material-ui/core/Card/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core';
import moment from 'moment';

const styles = {
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  gridContainer: {
    paddingTop: '50px'
  }
};

@withStyles(styles)
@observer
class Profile extends React.Component {
  componentDidMount() {
    mount();
  }

  componentWillUnmount() {
    unmount()
  }

  getDateString = (date) => {
    return moment(date).format('DD.MM.YYYY')
  };

  renderList = () => {
    const { classes } = this.props;

    if (_.isEmpty(profileStore.tests)) {
      return (
        <Grid item xs={12} md={6}>
          You have no tests yet
        </Grid>
      )
    }

    return profileStore.tests.map(item => (
      <Grid key={item.id} item xs={12} md={12}>
        <CardActionArea onClick={onClickTest(item.id)}>
          <Card className={classes.card}>
            <div className={classes.cardDetails}>
              <CardContent>
                <Typography component="h2" variant="h5">
                  {item.title}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  {this.getDateString(item.start_date)}
                  {' - '}
                  {this.getDateString(item.end_date)}
                </Typography>
                <Typography variant="subtitle1" paragraph>
                  {item.description}
                </Typography>
              </CardContent>
            </div>
          </Card>
        </CardActionArea>
      </Grid>
    ))
  };

  render() {
    const { classes } = this.props;

    return (
      <Layout>
        <Typography variant="h4">
          Username: {sessionStore.user.username}
        </Typography>
        <Grid container spacing={4} className={classes.gridContainer}>
          {this.renderList()}
        </Grid>
      </Layout>
    )
  }
}

export { Profile }
