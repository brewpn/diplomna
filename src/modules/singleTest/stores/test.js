import { observable, action } from 'mobx';
import moment from 'moment';

class Test {
  @observable
  details = {};
  @observable
  questions = [];
  @observable
  startedTime = null;
  @observable
  testStarted = false;

  @action
  setTestDetails(value) {
    this.details = value
  }

  @action
  startTest(value) {
    this.testStarted = value;
    this.startedTime = moment()
  }

  @action
  setQuestions(questions) {
    this.questions = questions
  }

  @action
  clean() {
    this.details = {};
    this.testStarted = false;
    this.startedTime = null;
    this.testStarted = false;
  }
}

const testStore = new Test();
export { testStore }
