import shortId from 'shortid';

import { testStore } from '../stores';
import { sessionStore } from '../../../stores';
import { TEST_LOAD_ACTION, SEND_TEST_RESULTS } from '../constants';
import { sendMapper } from '../utils';

import { action, getValueFromStorage, setValueToStorage, history } from '../../../utils';
import {QUESTION_TYPES, ROUTES} from '../../../constants';

export class TestService {
  @action({ action: TEST_LOAD_ACTION })
  async loadTest(id) {
    let tests = await getValueFromStorage('tests') || [];
    const results = await getValueFromStorage('results') || [];

    const test = tests.find(item => item.id === id) || {};
    const result = results.find(item => item.test_id === id && item.user_id === sessionStore.user.id) || {};

    if (!test) history.push(ROUTES.home);

    testStore.setTestDetails({
      ...test,
      score: result.score
    });
    testStore.setQuestions(test.questions)
  }

  @action({ action: SEND_TEST_RESULTS })
  async sendResults(value) {
    const tests = await getValueFromStorage('tests') || [];
    const results = await getValueFromStorage('results') || [];
    const neededTestIndex = tests.findIndex(item => item.id === testStore.details.id);
    const neededTest = tests[neededTestIndex];

    if (!neededTest) return;

    const mappedData = sendMapper(value);
    const score = await getScore(mappedData);

    tests[neededTestIndex] = {
      ...tests[neededTestIndex],
      score
    };

    const question = mappedData.questions
      .filter(item => item.type === QUESTION_TYPES[3].id)
      .map(item => ({
        id: shortId(),
        reviewed: false,
        value: item.answer
      }));

    results.push({
      id: shortId(),
      user_id: sessionStore.user.id,
      test_id: neededTest.id,
      score,
      code: question
    });

    await setValueToStorage('tests', tests);
    await setValueToStorage('results', results);

    await this.loadTest(testStore.details.id)
  }
}

const getScore = async (data) => {
  const tests = await getValueFromStorage('tests') || [];
  const neededTest = tests.find(item => item.id === testStore.details.id);
  let score = 0;

  neededTest.questions.forEach(question => {
    const neededQuestion = data.questions.find(item => item.id === question.id);

    if (question.type === QUESTION_TYPES[3].id) {
      return;
    }

    if (question.type === QUESTION_TYPES[0].id) {
      const correct = question.trueAnswers.every(
        item => neededQuestion.answer.some(
          answer => answer === question.answers[item].id
        )
      );

      if (correct) {
        score += 100 / neededTest.questions.length
      }

      return;
    }

    if (question.type === QUESTION_TYPES[2].id) {
      const correct = +question.trueAnswers === +neededQuestion.answer;

      if (correct) {
        score += 100 / neededTest.questions.length
      }

      return;
    }

    const correct = question.answers[question.trueAnswers].id === neededQuestion.answer;

    if (correct) {
      score += 100 / neededTest.questions.length
    }
  });

  return score;
};

const testService = new TestService();
export { testService };
