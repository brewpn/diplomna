import { matchPath } from 'react-router-dom';

import { testService } from '../requests';
import { testStore } from '../stores';

import { history } from '../../../utils';
import { ROUTES } from '../../../constants';

export const mount = () => {
  const match = matchPath(window.location.pathname, ROUTES.testDetails) || {};
  if (!(match.params || {}).id) {
    history.push(ROUTES.home);
    return;
  }

  testService.loadTest(match.params.id)
};

export const unmount = () => {
  testStore.clean();
};
