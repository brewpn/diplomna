import { testStore } from '../stores';
import { form, errorsStore } from '../../../stores';

import { TEST_FORM } from '../constants';
import {ROUTES} from '../../../constants';
import {setNestedValidation, history} from '../../../utils';
import { testValidation } from '../validation';
import { testService } from '../requests';

export const onCancel = () => {
  history.push(ROUTES.home)
};

export const onBeginTest = () => {
  testStore.startTest(true);
  form.merge(TEST_FORM, {
    questions: testStore.questions.map(question => ({ id: question.id, type: question.type }))
  });
};

export const onSubmitResults = async () => {
  errorsStore.clear(TEST_FORM);

  const value = form.fetchWithoutObservable(TEST_FORM);
  const valid = setNestedValidation(TEST_FORM, testValidation, value);

  if (valid) {
    await testService.sendResults(value);
    await testService.loadTest(testStore.details.id);
    testStore.startTest(false);
  }
};
