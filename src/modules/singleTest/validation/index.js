export const testValidation = {
  questions: {
    _validation: {
      answer: {
        presence: {
          allowEmpty: false,
          message: '^Field is required'
        }
      }
    }
  }
};