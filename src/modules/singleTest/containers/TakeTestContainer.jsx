import * as React from 'react';
import {observer} from 'mobx-react';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

import {
  Form,
  InputWrapper,
  Input,
  CheckboxList,
  RadioList,
  CodeEditor
} from '../../../components/Form';

import { QUESTION_TYPES } from '../../../constants';
import { TEST_FORM } from '../constants';
import { testStore } from '../stores';
import { onSubmitResults } from '../actions';

@withStyles(theme => ({
  paper: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(2)
  },
  score: {
    fontSize: '20px'
  },
  question: {
    marginTop: theme.spacing(2)
  },
  field: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2)
  },
  numberField: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
    width: '20%'
  },
  submit: {
    marginTop: theme.spacing(2),
    width: '100%'
  }
}))
@observer
class TakeTestContainer extends React.Component {

  getAnswerName = (index) => {
    return `questions.${index}.answer`
  };

  renderAnswers = (type, answers = [], index) => {
    const { classes, allDisabled } = this.props;

    switch(type) {
      case QUESTION_TYPES[0].id:
        return (
          <InputWrapper
            name={this.getAnswerName(index)}
            settings={{
              label: 'Answers',
              className: classes.field,
              options: answers,
              disabled: allDisabled
            }}
            Component={CheckboxList}
          />
        );

      case QUESTION_TYPES[1].id:
        return (
          <InputWrapper
            name={this.getAnswerName(index)}
            settings={{
              label: 'Answers',
              className: classes.field,
              options: answers,
              disabled: allDisabled
            }}
            Component={RadioList}
          />
        );

      case QUESTION_TYPES[2].id:
        return (
          <InputWrapper
            name={this.getAnswerName(index)}
            settings={{
              label: 'Answers',
              className: classes.numberField,
              type: 'number',
              disabled: allDisabled
            }}
            Component={Input}
          />
        );

      case QUESTION_TYPES[3].id:
        return (
          <InputWrapper
            name={this.getAnswerName(index)}
            settings={{
              disabled: allDisabled
            }}
            Component={CodeEditor}
          />
        );

      default:
        return null;
    }
  };

  render() {
    const { classes } = this.props;
    if (!testStore.testStarted) {
      return null;
    }

    return (
      <Form id={TEST_FORM}>
        {testStore.questions.map((question, index) => (
          <Paper className={classes.paper} key={`question:${question.id}`}>
            <Box>
              <Typography>{index+1}. {question.title}</Typography>
              {this.renderAnswers(question.type, question.answers, index)}
            </Box>
          </Paper>
        ))}
        <Button
          className={ classes.submit }
          variant="contained"
          color="secondary"
          onClick={ onSubmitResults }
        >
          Save Results
        </Button>
      </Form>
    );
  }
}

export { TakeTestContainer };
