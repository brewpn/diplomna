import * as React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import moment from 'moment';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import {withStyles} from '@material-ui/core/styles';

import { Layout } from '../../../components/Layout';
import { TakeTestContainer } from './TakeTestContainer';

import { mount, unmount, onBeginTest, onCancel } from '../actions';
import { testStore } from '../stores';

@withStyles(theme => ({
  dateValues: {
    marginBottom: 12
  },
  paper: {
    padding: theme.spacing(2)
  },
  begin: {
    marginLeft: theme.spacing(2)
  },
  score: {
    marginTop: theme.spacing(5)
  }
}))
@observer
class TestContainer extends React.Component {
  componentDidMount() {
    mount()
  }

  componentWillUnmount() {
    unmount()
  }

  renderScore = () => {
    const { classes } = this.props;
    const score = !_.isNil(testStore.details.score)
      ? `${testStore.details.score}/100`
      : 'none';
    const color = !_.isNil(testStore.details.score)
      ? 'textPrimary'
      : 'textSecondary';

    return (
      <Typography className={classes.score} color={color}>
        Score: {score}
      </Typography>
    )
  };

  render() {
    const { classes } = this.props;
    const details = testStore.details;

    const dateValues = `${moment(details.start_date).format('DD.MM.YYYY')} - ${moment(details.start_end).format('DD.MM.YYYY')}`;

    return (
      <Layout>
        <Paper className={classes.paper}>
          <Box display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h4">{details.title}</Typography>
            <Box display="flex">
              <Button variant="outlined" color="secondary" onClick={onCancel}>
                Cancel
              </Button>
              <Button
                className={classes.begin}
                variant="contained"
                color="secondary"
                onClick={onBeginTest}
                disabled={!!details.score || testStore.testStarted}
              >
                Begin test
              </Button>
            </Box>
          </Box>
          <Typography className={classes.dateValues} color="textSecondary">
            {dateValues}
          </Typography>
          <Typography>{details.description}</Typography>

          {this.renderScore()}
        </Paper>

        <TakeTestContainer/>
      </Layout>
    )
  }
}

export { TestContainer }
