import _ from 'lodash';

export const sendMapper = (value = []) => {
  const copy = _.cloneDeep(value);

  copy.questions.forEach((question, index) => {
    if (_.isArray(question.answer)) {
      copy.questions[index].answer = question.answer.map(item => item.id);
      return;
    }

    if (_.isString(question.answer) || _.isNumber(question.answer)) {
      return;
    }

    copy.questions[index].answer = question.answer.id
  });

  return copy
};
