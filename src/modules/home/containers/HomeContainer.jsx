import * as React from 'react';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { Layout } from '../../../components/Layout';
import { List, CreateTest } from '../components';
import { mount, unmount } from '../actions';

export class HomeContainer extends React.Component {
  componentDidMount() {
    mount();
  }

  componentWillUnmount() {
    unmount();
  }

  render() {
    return (
      <Layout>
        <Box display="flex" justifyContent="space-between">
          <Typography component="h4" variant="h4" align="left" color="textPrimary" gutterBottom>
            All tests
          </Typography>
          <CreateTest/>
        </Box>

        <List/>
      </Layout>
    )
  }
}
