import { listStore } from '../stores';
import { LIST_ACTION } from '../constants';

import { action, getValueFromStorage } from '../../../utils';

export class ListService {
  @action({ action: LIST_ACTION })
  async loadList() {
    let tests = await getValueFromStorage('tests') || [];

    listStore.setList(tests)
  }
}

const listService = new ListService();
export { listService };
