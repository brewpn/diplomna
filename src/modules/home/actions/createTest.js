import { history } from '../../../utils';
import { ROUTES } from '../../../constants';

export const onCreateTest = () => {
  history.push(ROUTES.createTest)
};