import { listService } from '../reqeusts';
import { listStore } from '../stores';

export const mount = () => {
  listService.loadList();
};

export const unmount = () => {
  listStore.clear()
};
