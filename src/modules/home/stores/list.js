import { observable, action } from 'mobx';

class ListStore {
  @observable
  list = [];

  @action
  setList(list) {
    this.list = list
  }

  @action
  clear() {
    this.list = [];
  }
}

const listStore = new ListStore();
export { listStore }
