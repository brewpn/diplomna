import * as React from 'react';
import {observer} from 'mobx-react';

import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import { onCreateTest } from '../actions';
import { sessionStore } from '../../../stores';

@observer
class CreateTest extends React.Component {
  state = {
    open: false
  };

  onCreate = () => {
    if (sessionStore.isLoggedIn) {
      onCreateTest();
      return;
    }

    this.handleTrigger(true)()
  };

  handleTrigger = (open) => () => {
    this.setState({
      open
    })
  };

  render() {
    const { open } = this.state;

    return (
      <ClickAwayListener onClickAway={this.handleTrigger(false)}>
        <Tooltip
          PopperProps={{
            disablePortal: true,
          }}
          onClose={this.handleTrigger(false)}
          open={open}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          title="You are not logged in"
        >
          <Button
            variant="outlined"
            color="secondary"
            onClick={this.onCreate}
          >
            Create test
          </Button>
        </Tooltip>
      </ClickAwayListener>
    )
  }
}

export { CreateTest }
