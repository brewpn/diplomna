import * as React from 'react';
import { observer } from 'mobx-react';
import moment from 'moment';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { listStore } from '../stores';
import { onClickTest } from '../actions';

const styles = {
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  gridContainer: {
    paddingTop: '50px'
  }
};

@withStyles(styles)
@observer
class List extends React.Component {
  getDateString = (date) => {
    return moment(date).format('DD.MM.YYYY')
  };

  renderGrid = () => {
    const { classes } = this.props;

    return listStore.list.map(item => (
      <Grid key={item.id} item xs={12} md={6}>
        <CardActionArea onClick={onClickTest(item.id)}>
          <Card className={classes.card}>
            <div className={classes.cardDetails}>
              <CardContent>
                <Typography component="h2" variant="h5">
                  {item.title}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  {this.getDateString(item.start_date)}
                  {' - '}
                  {this.getDateString(item.end_date)}
                </Typography>
                <Typography variant="subtitle1" paragraph>
                  {item.description}
                </Typography>
              </CardContent>
            </div>
          </Card>
        </CardActionArea>
      </Grid>
    ));
  };

  render() {
    const { classes } = this.props;

    if (_.isEmpty(listStore.list)) {
      return (
        <Grid container spacing={4} justify="center" className={classes.gridContainer}>
          <Typography>No results...</Typography>
        </Grid>
      );
    }

    return (
      <Grid container spacing={4} className={classes.gridContainer}>
        {this.renderGrid()}
      </Grid>
    )
  }
}

export { List }
