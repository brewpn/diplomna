import _ from 'lodash';

export const mapperCreate = (value) => {
  const copy = _.cloneDeep(value);
  copy.questions.forEach((question, index) => {
    copy.questions[index].type = question.type.id
  });

  return copy
};
