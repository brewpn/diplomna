import * as React from 'react';
import {observer} from 'mobx-react';

import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core';

import { form } from '../../../stores';

import { addQuestion } from '../actions';
import { CREATE_TEST_FORM_ID } from '../constants';

@withStyles({
  questionItem: {
    marginTop: '20px',
    width: '100%'
  }
})
@observer
class AddQuestion extends React.Component {
  render() {
    const { classes } = this.props;
    const questions = form.fetch(CREATE_TEST_FORM_ID, 'questions') || [];

    return (
      <Button
        className={classes.questionItem}
        variant="contained"
        color="secondary"
        onClick={addQuestion}
        disabled={questions.length >= 30}
      >
        ADD QUESTION
      </Button>
    )
  }
}

export { AddQuestion }
