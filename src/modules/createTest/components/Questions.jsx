import * as React from 'react';
import { observer } from 'mobx-react';

import { form } from '../../../stores';
import { CREATE_TEST_FORM_ID } from '../constants';
import { QUESTION_TYPES } from '../../../constants';
import { deleteQuestion, expandQuestion, onChangeType } from '../actions';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';

import { Answers } from './Answers';

import {
  InputWrapper,
  Input,
  Select
} from '../../../components/Form';

@withStyles((theme) => ({
  question: {
    marginTop: theme.spacing(2)
  },
  collapse: {
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  }
}))
@observer
class Questions extends React.Component {
  getFieldName = (index, name) => {
    return `questions.${index}.${name}`
  };

  showAnswers = (type = {}) => {
    return type.id === QUESTION_TYPES[0].id || type.id === QUESTION_TYPES[1].id
  };

  showNumberType = (type = {}) => {
    return type.id === QUESTION_TYPES[2].id
  };

  render() {
    const { classes } = this.props;

    const questions = form.fetch(CREATE_TEST_FORM_ID, 'questions') || [];
    const disableDeleteButton = questions.length === 1;

    return questions.map((question, index) => (
      <Card key={question.id} className={classes.question}>
        <CardHeader
          title={question.title || `Question #${index+1}`}
          subheader={''}
          action={(
            <React.Fragment>
              {!disableDeleteButton &&
                <IconButton onClick={ deleteQuestion(question.id) }>
                  <DeleteIcon/>
                </IconButton>
              }
              <IconButton onClick={expandQuestion(question.id)}>
                <ExpandMoreIcon/>
              </IconButton>
            </React.Fragment>
          )}
        />
        <Collapse in={question.expand}>
          <Box className={classes.collapse}>
            <InputWrapper
              name={this.getFieldName(index, 'title')}
              settings={{
                label: 'Title'
              }}
              Component={Input}
            />
            <InputWrapper
              name={this.getFieldName(index, 'type')}
              settings={{
                label: 'Type',
                options: QUESTION_TYPES,
                onChange: onChangeType(index)
              }}
              Component={Select}
            />
            {this.showAnswers(question.type) && (
              <Answers
                name={this.getFieldName(index, 'answers')}
                trueAnswersName={this.getFieldName(index, 'trueAnswers')}
                type={question.type}
                questionIndex={index}
              />
            )}
            {this.showNumberType(question.type) && (
              <InputWrapper
                name={this.getFieldName(index, 'trueAnswers')}
                settings={{
                  label: 'Answer',
                  type: 'number'
                }}
                Component={Input}
              />
            )}
          </Box>
        </Collapse>
      </Card>
    ))
  }
}

export { Questions }
