import * as React from 'react';
import { observer } from 'mobx-react';

import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import {
  Input,
  Checkbox,
  Radio,
  InputWrapper
} from '../../../components/Form';

import { form } from '../../../stores';
import { CREATE_TEST_FORM_ID } from '../constants';
import { QUESTION_TYPES } from '../../../constants';
import {deleteAnswer, addAnswer, onChangeTrueAnswer} from '../actions';

@withStyles(theme => ({
  container: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2)
  },
  button: {
    width: '100%'
  },
  input: {
    width: 'calc(100% - 70px)'
  },
  icon: {
    width: '48px',
    height: '48px'
  }
}))
@observer
class Answers extends React.Component {
  renderTrueAnswer = (index) => {
    const { type, trueAnswersName } = this.props;

    switch(type.id) {
      case QUESTION_TYPES[0].id:
        return (
          <InputWrapper
            name={ trueAnswersName }
            settings={{
              onChange: onChangeTrueAnswer,
              option: index
            }}
            Component={ Checkbox }
          />
        );

      case QUESTION_TYPES[1].id:
        return (
          <InputWrapper
            name={ trueAnswersName }
            settings={{
              option: index
            }}
            Component={ Radio }
          />
        );

      default:
        return null;
    }
  };

  renderAnswers = (answers) => {
    const { name, questionIndex, classes } = this.props;

    return answers.map((answer, index) => (
      <Box key={answer.id} display="flex" alignItems="center" justifyContent="space-between">
        <InputWrapper
          name={`${name}.${index}.title`}
          settings={{
            label: `Answer #${index+1}`,
            className: classes.input
          }}
          Component={Input}
        />

        {this.renderTrueAnswer(index)}

        <IconButton className={classes.icon} onClick={deleteAnswer(questionIndex, answer.id)}>
          <DeleteIcon/>
        </IconButton>
      </Box>
    ));
  };

  render() {
    const { name, classes, questionIndex } = this.props;
    const answers = form.fetch(CREATE_TEST_FORM_ID, name) || [];
    const disabled = answers.length >= 5;

    return (
      <Box className={classes.container}>
        <Typography variant="h5">Answers</Typography>
        {this.renderAnswers(answers)}
        <Button
          className={classes.button}
          variant="contained"
          color="secondary"
          disabled={disabled}
          onClick={addAnswer(questionIndex)}
        >
          Add answer
        </Button>
      </Box>
    )
  }
}

export { Answers };
