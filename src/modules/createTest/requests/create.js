import shortId from 'shortid';

import {action, setValueToStorage, getValueFromStorage, history} from '../../../utils';
import { CREATE_TEST_FORM_ID } from '../constants';
import {ROUTES} from '../../../constants';
import {sessionStore} from '../../../stores';

class CreateTest {
  @action({ action: CREATE_TEST_FORM_ID })
  async create(value) {
    const storage = await getValueFromStorage();

    await setValueToStorage('tests', [
      ...(storage.tests || []), {
        id: shortId(),
        user_id: sessionStore.user.id,
        ...value
      }
    ]);
    history.push(ROUTES.home)
  }
}

const createService = new CreateTest();
export { createService }
