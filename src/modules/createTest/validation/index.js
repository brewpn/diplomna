
export const testValidation = {
  _validation: {
    title: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    },
    end_date: {
      equality: {
        attribute: "start_date",
        message: "^From date should be lower than To",
        comparator: function(fromDate, toDate) {
          return fromDate.isSameOrAfter(toDate);
        }
      }
    },
    description: {
      length: { maximum: 1000 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    }
  },
  questions: {
    _validation: {
      title: {
        length: { maximum: 250 },
        presence: {
          allowEmpty: false,
          message: '^Field is required'
        }
      },
      type: {
        presence: {
          allowEmpty: false,
          message: '^Field is required'
        }
      }
    },
    answers: {
      _validation: {
        title: {
          length: { maximum: 250 },
          presence: {
            allowEmpty: false,
            message: '^Field is required'
          }
        }
      }
    }
  },
};