import * as React from 'react';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import { Questions, AddQuestion } from '../components';
import { Layout } from '../../../components/Layout';
import {
  Form,
  InputWrapper,
  Input,
  Date
} from '../../../components/Form'

import { CREATE_TEST_FORM_ID } from '../constants';

import { mount, onCancel, onSubmitTest } from '../actions';

@withStyles(theme => ({
  button: {
    marginLeft: theme.spacing(2)
  }
}))
class CreateTestContainer extends React.Component {
  componentDidMount() {
    mount()
  }

  render() {
    const { classes } = this.props;

    return (
      <Form id={CREATE_TEST_FORM_ID}>
        <Layout>
          <Grid container justify="space-between">
            <Typography variant="h4">New test</Typography>
            <Box display="flex">
              <Button className={classes.button} variant="outlined" color="secondary" onClick={onCancel}>
                Cancel
              </Button>
              <Button className={classes.button} variant="contained" color="secondary" onClick={onSubmitTest}>
                Save
              </Button>
            </Box>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={12} md={6}>
              <InputWrapper
                name="title"
                settings={{
                  label: "Test title*"
                }}
                Component={Input}
              />
              <Grid container justify="space-between">
                <InputWrapper
                  name="start_date"
                  settings={{
                    label: 'From*'
                  }}
                  Component={Date}
                />
                <InputWrapper
                  name="end_date"
                  settings={{
                    label: 'To*'
                  }}
                  Component={Date}
                />
              </Grid>
              <InputWrapper
                name="description"
                settings={{
                  label: "Description*",
                  multiline: true,
                  rows: 6,
                  rowsMax: 20
                }}
                Component={Input}
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <Questions/>
              <AddQuestion/>
            </Grid>
          </Grid>
        </Layout>
      </Form>
    )
  }
}

export { CreateTestContainer }
