import shortId from 'shortid';
import moment from 'moment';

import { form } from '../../../stores';
import { CREATE_TEST_FORM_ID } from '../constants';

export const mount = () => {
  form.merge(CREATE_TEST_FORM_ID, {
    start_date: moment(),
    end_date: moment(),
    questions: [{
      id: shortId(),
      title: '',
      expand: true
    }]
  });
};


