import { form, errorsStore } from '../../../stores';
import { history, setNestedValidation } from '../../../utils';

import { ROUTES } from '../../../constants';
import { CREATE_TEST_FORM_ID } from '../constants';
import { testValidation } from '../validation';
import { createService } from '../requests';
import { mapperCreate } from '../utils';

export const onSubmitTest = () => {
  errorsStore.clear(CREATE_TEST_FORM_ID);

  const values = form.fetchWithoutObservable(CREATE_TEST_FORM_ID);
  const valid = setNestedValidation(CREATE_TEST_FORM_ID, testValidation, values);

  if (valid) {
    const mappedData = mapperCreate(values);
    createService.create(mappedData);
  }
};

export const onCancel = () => {
  history.push(ROUTES.home)
};
