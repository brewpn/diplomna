import shortId from 'shortid';
import _ from 'lodash';

import { form } from '../../../stores';
import { CREATE_TEST_FORM_ID } from '../constants';

export const addQuestion = () => {
  const questions = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, 'questions') || [];
  questions.forEach(question => {
    question.expand = false
  });
  form.onChange(CREATE_TEST_FORM_ID, 'questions', [
    ...questions, {
      id: shortId(),
      title: '',
      expand: true
    }
  ])
};

export const deleteQuestion = (id) => () => {
  const questions = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, 'questions') || [];
  _.remove(questions, item => item.id === id);
  form.onChange(CREATE_TEST_FORM_ID, 'questions', questions)
};

export const expandQuestion = (id) => () => {
  const questions = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, 'questions') || [];
  questions.forEach(question => {
    if (question.id === id) {
      question.expand = !question.expand
    }
  });
  form.onChange(CREATE_TEST_FORM_ID, 'questions', questions)
};

export const onChangeType = (index) => ({name, value}) => {
  const question = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, `questions.${index}`);
  if ((question.type || {}).id !== value.id) {
    form.onChange(CREATE_TEST_FORM_ID, `questions.${index}`, {
      ...question,
      type: value,
      trueAnswers: null
    })
  }
};
