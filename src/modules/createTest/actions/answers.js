import _ from 'lodash';
import shortId from 'shortid';

import {form} from '../../../stores';
import {CREATE_TEST_FORM_ID} from '../constants';

export const deleteAnswer = (questionIndex, answerId) => () => {
  const name = `questions.${questionIndex}.answers`;

  const answers = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, name) || [];
  _.remove(answers, item => item.id === answerId);
  form.onChange(CREATE_TEST_FORM_ID, name, answers)
};

export const addAnswer = (questionIndex) => () => {
  const name = `questions.${questionIndex}.answers`;
  const answers = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, name) || [];
  form.onChange(CREATE_TEST_FORM_ID, name, [
    ...answers, {
      id: shortId(),
      title: ''
    }
  ])
};

export const onChangeTrueAnswer = ({name, value}) => {
  const trueAnswers = form.fetchWithoutObservable(CREATE_TEST_FORM_ID, name) || [];
  const removed = _.remove(trueAnswers, item => item === value);

  if (_.isEmpty(removed)) {
    trueAnswers.push(value);
  }

  form.onChange(CREATE_TEST_FORM_ID, name, trueAnswers);
};
