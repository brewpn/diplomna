import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import { ROUTES } from '../../../constants';

import { AuthContainer } from './AuthContainer';
import { RegisterContainer } from './RegisterContainer';

export class AuthRouter extends React.Component {
  render() {
    return (
      <Switch>
        <Route path={ROUTES.login} component={AuthContainer}/>
        <Route path={ROUTES.register} component={RegisterContainer}/>
      </Switch>
    )
  }
}
