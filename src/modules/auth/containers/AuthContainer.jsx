import * as React from 'react';
import { Link } from 'react-router-dom';

import { ROUTES } from '../../../constants';
import { AUTH_FORM_ID } from '../constants';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

import { initialize, unmount, onLogin, onClickHome } from '../actions';

import {
  Form,
  InputWrapper,
  Input
} from '../../../components/Form';

@withStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(20),
    width: '100%'
  },
  button: {
    width: '100%',
    marginTop: '20px'
  },
  linkBlock: {
    marginTop: '10px',
  },
  link: {
    color: theme.palette.secondary.main
  },
  home: {
    cursor: 'pointer'
  }
}))
class AuthContainer extends React.Component {
  componentDidMount() {
    initialize();
  }

  componentWillUnmount() {
    unmount();
  }

  render() {
    const { classes } = this.props;

    return (
      <Form id={AUTH_FORM_ID}>
        <Container component="main" maxWidth="xs">
          <CssBaseline/>

          <Box display="flex" flexDirection="column" alignItems="center" className={classes.paper}>
            <Typography className={classes.home} variant="h2" component="h2" onClick={onClickHome}>
              Test App
            </Typography>

            <InputWrapper
              name="username"
              settings={{
                label: 'Username'
              }}
              Component={Input}
            />
            <InputWrapper
              name="password"
              settings={{
                label: 'Password',
                type: 'password'
              }}
              Component={Input}
            />

          </Box>
          <Button variant="contained" color="secondary" className={classes.button} onClick={onLogin}>
            SIGN IN
          </Button>
          <div className={classes.linkBlock}>
            <Link to={ROUTES.register} className={classes.link}>
              {"Don't have an account? Sign Up"}
            </Link>
          </div>
        </Container>
      </Form>
    );
  }
}

export {AuthContainer}
