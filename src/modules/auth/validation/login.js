export const LOGIN_VALIDATION = {
  _validation: {
    username: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    },
    password: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    }
  }
};