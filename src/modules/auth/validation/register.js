export const REGISTER_VALIDATION = {
  _validation: {
    username: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    },
    password: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      }
    },
    passwordConfirmation: {
      length: { maximum: 250 },
      presence: {
        allowEmpty: false,
        message: '^Field is required'
      },
      equality: {
        attribute: "password",
        message: "^Passwords are not the same",
        comparator: function(v1, v2) {
          return JSON.stringify(v1) === JSON.stringify(v2);
        }
      }
    }
  }
};