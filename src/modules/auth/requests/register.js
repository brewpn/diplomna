import { toast } from 'react-toastify';
import shortId from 'shortid';

import { action, getValueFromStorage, setValueToStorage } from '../../../utils';
import { REGISTER_FORM_ID } from '../constants';

class Register {
  @action({ action: REGISTER_FORM_ID })
  async register(value) {
    const users = await getValueFromStorage('users') || [];
    const exists = users.find(item => item.username === value.username);

    if (exists) {
      toast.error('User is already exists');
      return false
    }

    await setValueToStorage('users', [
      ...users, {
        id: shortId(),
        ...value
      }
    ]);

    return true;
  }
}

const registerService = new Register();
export { registerService }
