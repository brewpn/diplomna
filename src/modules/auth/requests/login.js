import {toast} from 'react-toastify';

import { action, getValueFromStorage } from '../../../utils';
import { REGISTER_FORM_ID } from '../constants';
import { sessionStore } from '../../../stores';

class Login {
  @action({ action: REGISTER_FORM_ID })
  async login(userValue = {}) {
    const users = await getValueFromStorage('users') || [];
    const user = users.find(
      item => item.username === userValue.username && item.password === userValue.password
    );

    if (user) {
      sessionStore.login(user, user.id);
      return true
    }

    toast.error('Invalid username or password');
    return false
  }
}

const loginService = new Login();
export { loginService }
