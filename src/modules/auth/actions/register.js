import shortId from 'shortid';

import {errorsStore, form} from '../../../stores';

import {history, setNestedValidation} from '../../../utils';
import {ROUTES} from '../../../constants';

import {REGISTER_FORM_ID} from '../constants';
import {REGISTER_VALIDATION} from '../validation';
import { registerService, loginService } from '../requests';

export const onRegister = async () => {
  errorsStore.clear(REGISTER_FORM_ID);

  const value = form.fetchWithoutObservable(REGISTER_FORM_ID);
  const valid = setNestedValidation(REGISTER_FORM_ID, REGISTER_VALIDATION, value);

  if (valid) {
    const status = await registerService.register(value);

    if (status) {
      await loginService.login(value);
      history.push(ROUTES.home)
    }
  }
};
