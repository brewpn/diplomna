import { form, errorsStore } from '../../../stores';

import { history } from '../../../utils';
import { ROUTES } from '../../../constants';

import { setNestedValidation } from '../../../utils';

import { LOGIN_VALIDATION } from '../validation';
import { AUTH_FORM_ID } from '../constants';
import { loginService } from '../requests';

export const onLogin = async () => {
  errorsStore.clear(AUTH_FORM_ID);

  const value = form.fetchWithoutObservable(AUTH_FORM_ID);
  const valid = setNestedValidation(AUTH_FORM_ID, LOGIN_VALIDATION, value);

  if (valid) {
    const status = await loginService.login(value);

    if (status) {
      history.push(ROUTES.home)
    }
  }
};

export const onClickHome = () => {
  history.push(ROUTES.home)
};
