import { showHeader, hideHeader } from '../../../components/Header';

export const initialize = () => {
  hideHeader()
};

export const unmount = () => {
  showHeader()
};
