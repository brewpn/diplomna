import {action, observable} from 'mobx';

class Test {
  @observable
  test = {};

  @observable
  results = [];

  @action
  setTest(data) {
    this.test = data
  }

  @action
  setResults(data) {
    this.results = data
  }

  @action
  clear() {
    this.test = {};
    this.results = [];
  }
}

const testStore = new Test();
export {testStore}
