import { testService } from '../requests';

export const onSubmit = (trigger, id) => () => {
  testService.submitDecision(trigger, id)
};
