import { matchPath } from 'react-router-dom';

import { testService } from '../requests';
import { testStore } from '../stores';

import { history } from '../../../utils';
import {ROUTES} from '../../../constants';

export const mount = async () => {
  const match = matchPath(window.location.pathname, ROUTES.profileTest) || {};
  if (!(match.params || {}).id) {
    history.push(ROUTES.home);
    return;
  }

  await testService.getTestData(match.params.id)
};

export const unmount = () => {
  testStore.clear();
};
