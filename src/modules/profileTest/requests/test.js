import { action, getValueFromStorage, setValueToStorage } from '../../../utils';

import { testStore } from '../stores';
import { sessionStore } from '../../../stores';
import { LOAD_USER_TEST, SUBMIT_DECISION } from '../constants';

class Test {
  @action({ action: LOAD_USER_TEST })
  async getTestData(id) {
    const tests = await getValueFromStorage('tests') || [];
    const users = await getValueFromStorage('users') || [];
    const results = await getValueFromStorage('results') || [];
    const test = tests.find(item => item.id === id && item.user_id === sessionStore.user.id) || {};

    if (test) {
      const filteredResults = results
        .filter(item => item.test_id === test.id)
        .map(item => ({
          ...item,
          user: users.find(user => user.id === item.user_id)
        }));

      testStore.setTest(test);
      testStore.setResults(filteredResults);
    }
  }

  @action({ action: SUBMIT_DECISION })
  async submitDecision(trigger, id) {
    const questions = testStore.test.questions || [];
    const results = await getValueFromStorage('results');
    const index = results.findIndex(item => item.test_id === testStore.test.id);
    const neededResult = results[index];
    const value = 100 / questions.length;

    if (neededResult) {
      results[index].score = trigger
        ? +neededResult.score + value
        : +neededResult.score || 0;
      const codeIndex = (results[index].code || []).findIndex(item => item.id === id);
      results[index].code[codeIndex] = {
        ...results[index].code[codeIndex],
        reviewed: true
      };

      await setValueToStorage('results', results);

      await this.getTestData(testStore.test.id);
    }
  }
}

const testService = new Test();
export {testService}
