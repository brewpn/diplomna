import * as React from 'react';
import {observer} from 'mobx-react';

import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import { Layout } from '../../../components/Layout';
import {List} from '../components';
import {mount, unmount} from '../actions';
import { sessionStore } from '../../../stores';
import {Box} from '@material-ui/core';

@withStyles(theme => ({
  box: {
    paddingTop: theme.spacing(2)
  }
}))
@observer
class ProfileTest extends React.Component {
  componentDidMount() {
    mount();
  }

  componentWillUnmount() {
    unmount()
  }

  render() {
    const { classes } = this.props;

    return (
      <Layout>
        <Typography variant="h4">
          Username: {sessionStore.user.username}
        </Typography>
        <Box className={classes.box}>
          <List/>
        </Box>
      </Layout>
    )
  }
}

export { ProfileTest }
