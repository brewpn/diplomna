import * as React from 'react';
import { observer } from 'mobx-react';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import { testStore } from '../stores';
import { onSubmit } from '../actions';

import {Answer} from './';

@withStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  code: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  button: {
    width: '48%'
  },

}))
@observer
class List extends React.Component {
  getOptions = (result) => {
    if (result.code) {
      return {
        expandIcon: <ExpandMoreIcon />,
        ariaControls: 'panel1a-content'
      }
    }

    return {}
  };

  render() {
    const { classes } = this.props;

    if (_.isEmpty(testStore.results)) {
      return 'No results'
    }

    return testStore.results.map(result => (
      <ExpansionPanel key={`result:${result.id}`}>
        <ExpansionPanelSummary
          {...this.getOptions(result)}
          id="panel1a-header"
        >
          <Typography className={classes.heading}>{result.user.username} - score: {result.score || 0}/100</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Box className={classes.code}>
            {(result.code || []).map(code => (
              <React.Fragment>
                <Answer code={ code.value }/>

                {!code.reviewed &&
                  <Box display="flex" justifyContent="space-between">
                    <Button onClick={ onSubmit(true, code.id) } className={ classes.button } variant="contained"
                            color='primary'>Approve</Button>
                    <Button onClick={ onSubmit(false, code.id) } className={ classes.button } variant='contained'>Reject</Button>
                  </Box>
                }
              </React.Fragment>
            ))}
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    ));
  }
}

export { List }
