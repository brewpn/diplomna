import * as React from 'react';
import {
  CodeEditor
} from '../../../components/Form';

export class Answer extends React.Component {
  static defaultProps = {
    code: ''
  };

  render() {
    const { code } = this.props;

    return (
      <CodeEditor
        value={code}
        disabled
      />
    )
  }
}
