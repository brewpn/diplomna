import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import { ROUTES } from '../constants';

import { HomeContainer } from '../modules/home';
import { AuthRouter } from '../modules/auth';
import { CreateTestContainer } from '../modules/createTest';
import { TestContainer } from '../modules/singleTest';
import { Profile } from '../modules/profile';
import { ProfileTest } from '../modules/profileTest';

export class User extends React.Component {
  render() {
    return (
      <Switch>
        <Route exect path={ROUTES.profileTest} component={ProfileTest}/>
        <Route exect path={ROUTES.profile} component={Profile}/>
        <Route path={ROUTES.auth} component={AuthRouter}/>
        <Route path={ROUTES.createTest} component={CreateTestContainer}/>
        <Route path={ROUTES.testDetails} component={TestContainer}/>
        <Route exect path={ROUTES.home} component={HomeContainer}/>
      </Switch>
    )
  }
}
